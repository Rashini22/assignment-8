/*Assignment 8  :Store and Print Students' Details by using Structure,Loops,Arrays
Name            :P.Rashini Lakshika
Registration No :202057*/

#include<stdio.h>

struct student{ //Structure Definition
    char name[20];
    char subject[10];
    int marks;
};

int main()
{
    int c,i;
    int num1=1;
    int num2=1;
    printf("Enter the Total Number of Students: ");
    scanf("%d",&c);

    struct student n[c];

    printf("\nEnter Student Details\n");//Storing Student Details by User Inputs
    printf("------------------------\n");
        for(i=0; i<c; i++)
            {
                printf("Student No: %d\n",num1);
                printf("\tEnter Name : ");
                scanf("%s",n[i].name);
                printf("\tEnter Subject : ");
                scanf("%s",n[i].subject);
                printf("\tEnter Marks :");
                scanf("%d",&n[i].marks);
                num1++;

                printf("\n");
            }

    printf("------------------------------------------------------------\n");

    printf("\nPrint the Student Details \n");//Printing Student Details
    printf("---------------------------\n");
        for(i=0; i<c; i++)
            {
                printf("Student No\t:%d\n",num2);
                printf("First Name\t:%s\n",n[i].name);
                printf("Subject\t\t:%s\n",n[i].subject);
                printf("Marks\t\t:%d\n",n[i].marks);
                num2++;
                printf("\n");

            }

    return 0;
}
